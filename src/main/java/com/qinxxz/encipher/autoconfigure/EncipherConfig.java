package com.qinxxz.encipher.autoconfigure;

import com.qinxxz.encipher.core.Constant;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * springboot配置
 *
 * @author qinxianzhong
 * @since 2023/8/25
 */
@ConfigurationProperties(EncipherConfig.PREFIX)
public record EncipherConfig(String secretKey,String vectorKey,String method) {

    static final String PREFIX = "spring.encipher";

    public EncipherConfig{


        if (method == null || method.length() == 0) {
            throw new RuntimeException("AES加密方式不能为空！");
        }

        if (!method.equals("CBC") && !method.equals("ECB")) {
            throw new RuntimeException("加密方式必须为 CBC或ECB");
        }

        if (secretKey.length() != 16 && secretKey.length() != 32) {
            throw new RuntimeException("secretKey:指定的密钥大小对此算法无效 16位 或 32位");
        }


        if (method.equals(Constant.CBC)) {
            if (vectorKey == null || vectorKey.length() != 16) {
                throw new RuntimeException("vectorKey:指定的密钥大小对此算法无效，必须16位");
            }
        }
    }
}
