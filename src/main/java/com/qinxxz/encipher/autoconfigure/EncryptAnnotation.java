package com.qinxxz.encipher.autoconfigure;

import com.qinxxz.encipher.annotation.ApiEncrypt;
import com.qinxxz.encipher.annotation.Decryption;
import com.qinxxz.encipher.annotation.Encryption;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.pattern.PathPattern;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 加密注解扫描器
 *
 * @author qinxianzhong
 * @since 2023/8/17 17:53:04
 */
public class EncryptAnnotation implements ApplicationContextAware {
    /**
     * 解密注解url路径集合
     */
    public static List<String> decryptionList = new ArrayList<>();

    /**
     * 加密注解url路径集合
     */
    public static List<String> encryptionList = new ArrayList<>();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        //获取使用RestController注解标注的的所有controller类
        Map<String, Object> controllers = applicationContext.getBeansWithAnnotation(RestController.class);
        //遍历每个controller层
        for (Map.Entry<String, Object> entry : controllers.entrySet()) {
            Object value = entry.getValue();
            ApiEncrypt decrypt = AnnotationUtils.findAnnotation(value.getClass(), ApiEncrypt.class);
            if (decrypt != null) {
                Class<?> cls = value.getClass();
                Method[] methods = cls.getMethods();
                for (Method method : methods) {
                    String url = getUrl(cls, method);
                    decryptionList.add(url);
                    encryptionList.add(url);
                }
            }

        }


        Map<RequestMappingInfo, HandlerMethod> handlerMethodMap = applicationContext.getBean(RequestMappingHandlerMapping.class).getHandlerMethods();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> infoEntry : handlerMethodMap.entrySet()) {
            HandlerMethod handlerMethod = infoEntry.getValue();
            Decryption decryption = handlerMethod.getMethodAnnotation(Decryption.class);
            Encryption encryption = handlerMethod.getMethodAnnotation(Encryption.class);
            //获取uri路径地址
            Set<PathPattern> patterns = null;
            if (infoEntry.getKey().getPathPatternsCondition() != null) {
                patterns = infoEntry.getKey().getPathPatternsCondition().getPatterns();
            }
            if (patterns != null) {
                for (PathPattern url : patterns) {
                    if (decryption != null) {

                        if (!decryptionList.contains(url.getPatternString())) {
                            decryptionList.add(url.getPatternString());
                        }
                    }
                    if (encryption != null) {
                        if (!encryptionList.contains(url.getPatternString())) {
                            encryptionList.add(url.getPatternString());
                        }
                    }
                }
            }
        }
    }

    /**
     * 获取Controller下方法的请求地址
     * @param cls Controller类
     * @param method Controller下的方法
     * @return 请求地址
     */
    private String getUrl(Class<?> cls,Method method){
        StringBuilder uri = new StringBuilder();

        RequestMapping reqMapping = AnnotationUtils.findAnnotation(cls, RequestMapping.class);
        if (reqMapping != null) {
            uri.append(formatUri(reqMapping.value()[0]));
        }

        GetMapping getMapping = AnnotationUtils.findAnnotation(method, GetMapping.class);
        PostMapping postMapping = AnnotationUtils.findAnnotation(method, PostMapping.class);
        RequestMapping requestMapping = AnnotationUtils.findAnnotation(method, RequestMapping.class);
        PutMapping putMapping = AnnotationUtils.findAnnotation(method, PutMapping.class);
        DeleteMapping deleteMapping = AnnotationUtils.findAnnotation(method, DeleteMapping.class);

        if (getMapping != null) {
            uri.append(formatUri(getMapping.value()[0]));

        } else if (postMapping != null) {
            uri.append(formatUri(postMapping.value()[0]));

        } else if (putMapping != null) {
            uri.append(formatUri(putMapping.value()[0]));

        } else if (deleteMapping != null) {
            uri.append(formatUri(deleteMapping.value()[0]));

        } else if (requestMapping != null) {
            uri.append(formatUri(requestMapping.value()[0]));

        }
        return  uri.toString();
    }


    private String formatUri(String uri) {
        if (uri.startsWith("/")) {
            return uri;
        }
        return "/" + uri;
    }



}
