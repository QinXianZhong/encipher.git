package com.qinxxz.encipher.autoconfigure;


import com.qinxxz.encipher.core.EncryptionFactory;
import com.qinxxz.encipher.core.IEncryption;
import jakarta.annotation.Resource;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动配置
 *
 * @author qinxianzhong
 * @since 2023/8/17 16:24:41
 */
@Configuration
@EnableConfigurationProperties(EncipherConfig.class)
public class EncipherAutoConfigure {

    @Resource
    private EncipherConfig encipherConfig;


    @Bean
    public FilterRegistrationBean<EncipherFilter> registrationBean() {
        FilterRegistrationBean<EncipherFilter> registrationBean = new FilterRegistrationBean<EncipherFilter>();
        IEncryption encryption = EncryptionFactory.makeEncryption(encipherConfig.method());
        registrationBean.setFilter(new EncipherFilter(encipherConfig,encryption));
        registrationBean.setOrder(1);
        registrationBean.setName("EncipherFilter");
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }

    @Bean
    public EncryptAnnotation annotationSelect() {
        return new EncryptAnnotation();
    }


}
