package com.qinxxz.encipher.core;

/**
 * 常量
 * @author qinxianzhong
 * @since 2023/8/11 10:37:09
 */
public class Constant {

    /**
     * AES加密
     */
    public static final String AES = "AES";

    /**
     * CBC加密模式
     */
    public static final String CBC = "CBC";

    /**
     * ECB加密模式
     */
    public static final String ECB = "ECB";

    /**
     * 编码
     */
    public static final String ENCODING = "UTF-8";


    /**
     * AES加密/CBC向量工作模式/PKCS填充方式
     */
    public static final String AES_CBC_PKCS = "AES/CBC/PKCS5Padding";

    /**
     * AES加密/ECB/PKCS填充方式
     */
    public static final String AES_ECB_PKCS = "AES/ECB/PKCS5Padding";

}
