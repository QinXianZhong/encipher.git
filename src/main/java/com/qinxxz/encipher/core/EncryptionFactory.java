package com.qinxxz.encipher.core;

/**
 * @author qinxianzhong
 * @since 2023/9/16
 */
public class EncryptionFactory {

    public static IEncryption makeEncryption(String method){
        if (method.equals(Constant.CBC)) {
            return new CbcEncryption();
        } else if (method.equals(Constant.ECB)) {
            return new EcbEncryption();
        }
        return null;
    }

}
