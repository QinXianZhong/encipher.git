package com.qinxxz.encipher.core;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * AES加密 ECB加密方法
 * @author qinxianzhong
 * @since 2023/9/16
 */
public class EcbEncryption implements IEncryption {


    /**
     * AES加密
     *
     * @param content   要加密的内容
     * @param secretKey 密钥
     * @return 密文
     */
    @Override
    public String encryption(String content,String secretKey,String vectorKey){
       try{
           //实例化加密对象
           Cipher cipher = Cipher.getInstance(Constant.AES_ECB_PKCS);

           //实例化加密密钥
           SecretKeySpec spec = new SecretKeySpec(secretKey.getBytes(Constant.ENCODING), Constant.AES);

           //操作模式为加密
           cipher.init(Cipher.ENCRYPT_MODE, spec);

           //加密内容
           byte[] encryptByte = cipher.doFinal(content.getBytes(Constant.ENCODING));

           //加密内容转换成base64
           return Base64.getEncoder().encodeToString(encryptByte);
       }catch (Exception e){
           throw new RuntimeException("AES加密失败！" + e);
       }
    }

    /**
     * 解密
     *
     * @param content   加密密文
     * @param secretKey 密钥
     * @return 明文
     */
    @Override
    public String decryption(String content,String secretKey,String vectorKey){
       try{
           //实例化加密对象
           Cipher cipher = Cipher.getInstance(Constant.AES_ECB_PKCS);

           //实例化解密密钥
           SecretKeySpec spec = new SecretKeySpec(secretKey.getBytes(Constant.ENCODING), Constant.AES);

           //操作模式为解密
           cipher.init(Cipher.DECRYPT_MODE,spec);

           //base64密文解码
           byte[] base64 = Base64.getDecoder().decode(content.replaceAll(" +", "+").getBytes(Constant.ENCODING));

           //再使用cipher解密
           return new String(cipher.doFinal(base64), Constant.ENCODING);
       }catch (Exception e){
           throw new RuntimeException("AES解密失败！" + e);
       }
    }

}
