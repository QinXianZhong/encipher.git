package com.qinxxz.encipher.core;

/**
 * @author qinxianzhong
 * @since 2023/9/16
 */
public interface IEncryption {


    String encryption(String content,String secretKey,String vectorKey);


    String decryption(String content,String secreKey,String vectorKey);

}
